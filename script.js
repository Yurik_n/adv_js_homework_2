console.log("is working");

/*

1) Наведіть кілька прикладів, коли доречно використовувати в коді
 конструкцію try...catch.

Після винекнення помилки, щоб код продовжував працювати, використовують 
конструкцію try...catch для перехоплення та оброблення виникаючих помилок.
Наприклад коли з серверу завантажилися не коректні дані, тобто ті дані які 
не можуть бути опрацьовані.
 Робота з файлами, коли після відкриття файла не відбулося запису до файлу(виникла помилка),
 але закрити його потрібно - допоможе блок finaly конструкції try...catch.
Також можна генерувати свої помилки через оператор throw та опрацьовувати
їх try...catch-ом 
 */

/*
Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price).
 Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.
*/




const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

const parent = document.querySelector("#root") 
const ulList = document.createElement("ul")

books.forEach(el => {    
    try {
        const li = document.createElement("li")
        if (el.name && el.author && el.price) {
            li.innerText = `author: ${el.author}, name: ${el.name}, price: ${el.price}`
            ulList.append(li)
        }else if(!el.name){
            throw new Error("Element doesn`t have name-property ");
        }else if(!el.author){
            throw new Error("Element doesn`t have author-property ");
        }else if(!el.price){
            throw new Error("Element doesn`t have price-property ");
        }
    } catch (er) {
        console.log(er);
    }
});

parent.append(ulList)


